﻿using HtmlAgilityPack;
using System;
using System.Net;
using System.Text;

namespace GToolsLib
{

    public static class GTools
    {

        public enum GToolsLang
        {
            DE,
            EN
        }

        public static long GetSearchHitsByTerm(string searchTerm, GToolsLang language)
        {

            long tmpSearchHitsCount = -1;

            // Check given parameter.
            if (string.IsNullOrWhiteSpace(searchTerm) || string.IsNullOrWhiteSpace(language.ToString()))
            {
                return tmpSearchHitsCount;
            }

            try
            {

                // Build URI for search parameter.
                string tmpSearchUri = "http://google.com/search?q=" + searchTerm.Trim();

                // Get website with webclient.
                WebClient tmpWebClient = new WebClient();

                tmpWebClient.Encoding = Encoding.Default;
                string tmpPage = tmpWebClient.DownloadString(tmpSearchUri);

                if (tmpPage != null)
                {
                    // Load website into HtmlAgilityPack document.
                    HtmlDocument tmpDocument = new HtmlDocument();

                    tmpDocument.LoadHtml(tmpPage);

                    // Parse HTML doc to get search stats.
                    var tmpStats = tmpDocument?.DocumentNode?.SelectSingleNode("//div[@id='resultStats']");

                    if (tmpStats != null && tmpStats.InnerText != null)
                    {

                        string tmpHitCounter = string.Empty;

                        // Remove not needed number surrounding strings.
                        switch (language)
                        {

                            case GToolsLang.DE:
                                tmpHitCounter = tmpStats.InnerText.Replace("Ungefähr ", "").Replace(" Ergebnisse", "").Replace(".", "");
                                break;

                            case GToolsLang.EN:
                                tmpHitCounter = tmpStats.InnerText.Replace("About ", "").Replace(" results", "").Replace(".", "");
                                break;

                            default:
                                break;

                        }

                        // Parse result to long.
                        if (!string.IsNullOrWhiteSpace(tmpHitCounter))
                        {
                            tmpSearchHitsCount = long.Parse(tmpHitCounter);
                        }
                        
                    }
                   
                }

            }
            catch (Exception ex)
            {
                throw;
            }

            return tmpSearchHitsCount;
        }

    }

}
